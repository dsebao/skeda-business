
//Create loading spinner
function loadingAjax(){
	var loading = $('.globalcover');
	$(document).ajaxStart(function () {
		$('html, body').css("cursor", "wait");
	    loading.fadeIn('fast');
	}).ajaxStop(function () {
	    loading.hide();
	    $('html, body').css("cursor", "auto");
	});
}

jQuery(document).ready(function($) {
	loadingAjax();
});

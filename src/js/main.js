/*
AJAX - Send data using ajax
Receive data, use one callback for beforesend and another for handle output
*/


function ajaxSend(mydata,beforeSend,handleData,type = 'POST',datatype = 'json',globaltype = true){
    $.ajax({
        type: type,
        language: 'es_ES',
        url: jsvar.ajaxurl,
        dataType: datatype,
        data: mydata,
        global: globaltype,
        beforeSend: function (data) {
            beforeSend(data);
        },
        success: function(data){
            console.log(data);
            handleData(data);
        },
        error: function(e){
            console.log(e);
            $.notify(e,{type: 'danger'});
        }
    });
}

function nav(){
    $('.navhoverdropdown').bootnavbar();
    var navm = $('#dash-menu');
    var url = navm.data('page');
    navm.find('a').each(function(){
    	var d = $(this);
		var href = d.attr('href');
		if (href === url)
			d.parent().addClass('active');
		else
			d.parent().removeClass('active');
	});
}



function ajaxForms(){
    var nonce = $('meta[name="csrf-token"]').attr('content');
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': nonce}});

    $('form.s_form').validator().submit(function(event) {
        var form = $(this);

        if (event.isDefaultPrevented()) {
        } else{
            event.preventDefault();
            ajaxSend(form.serialize() + '&_wpn=' + jsvar.ajax_nonce,function(data){
                //Before send
            },function(out){
                if(out.success){
                } else {
                }
                if(out.resetform)
                    form.trigger('reset');
            });
        }
    });
}


jQuery(document).ready(function($) {

	nav();

	ajaxForms();
});
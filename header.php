<?php

$a = getAmeliaBusinessData();
$cover = skeda_getcompany('skeda_data_cover');
$logo = (skeda_getcompany('skeda_data_logo_id') != '') ? wp_get_attachment_thumb_url(skeda_getcompany('skeda_data_logo_id')) : '';

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Google Fonts -->
        <link href='https://unpkg.com/boxicons@2.0.2/css/boxicons.min.css' rel='stylesheet'>
        <title><?php echo skeda_getcompany('skeda_data_name') ." | " . wp_title( '|', true, 'right' );?></title>
        <?php wp_head();?>
    </head>
    <body id="front" <?php body_class('u-custombox-no-scroll');?>>
        <?php do_action('after_body_open_tag'); ?>
        <div class="container-fluid">
            <div class="row">
                <nav class="col-md-2 bg-light sidebar navbar-expand-md">
                    <div class="pt-0 pt-md-2">
                        <div class="logo text-left text-md-center py-3 px-2 px-md-0">
                            <div class="row">
                                <div class="col-9 col-md-12">
                                    <a href="<?php bloginfo('url');?>/" class="">
                                        <?php if($logo != ''){?>
                                        <img width="170px" height="170px" src="<?php echo $logo;?>" alt="" class="rounded-circle">
                                        <?php } else {?>
                                            <svg width="170px" height="170px" viewBox="0 0 110 110" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Group"><circle id="Oval" fill="#d4cee7" cx="55" cy="55" r="55"></circle><g id="1540903" transform="translate(34.000000, 32.000000)" fill="#FFFFFF" fill-rule="nonzero"><path d="M36.6953125,28.8666077 C36.6953125,30.2906189 35.5411987,31.4447327 34.1171875,31.4447327 C32.6931763,31.4447327 31.5390625,30.2906189 31.5390625,28.8666077 C31.5390625,27.4429322 32.6931763,26.2884827 34.1171875,26.2884827 C35.5411987,26.2884827 36.6953125,27.4429322 36.6953125,28.8666077 Z M43.9979858,28.8733216 L43.9979858,29.3882751 C43.9979858,32.1167908 42.4836731,35.2478027 39.497345,38.6950378 C37.3831482,41.1351928 35.2904358,42.847229 35.2021484,42.9190674 L34.1171875,43.8022766 L33.0322266,42.9190674 C32.9442749,42.847229 30.8512268,41.1351928 28.73703,38.6950378 C25.7507019,35.2478027 24.2363892,32.1167908 24.2363892,29.3882751 L24.2363892,28.8733216 C24.2363892,23.4250183 28.6688842,18.9921875 34.1171875,18.9921875 C39.5654908,18.9921875 43.9979858,23.4250183 43.9979858,28.8733216 Z M40.5604858,28.8733216 C40.5604858,25.320343 37.670166,22.4296875 34.1171875,22.4296875 C30.564209,22.4296875 27.6738892,25.320343 27.6738892,28.8733216 L27.6738892,29.3882751 C27.6738892,32.7042542 31.6568909,37.0531616 34.1171875,39.3083496 C36.5771484,37.0534973 40.5604858,32.7042542 40.5604858,29.3882751 L40.5604858,28.8733216 Z M39.2616882,0 L4.73596191,0 L0,15.8185424 L0,16.0703125 C0,17.6081238 0.606597922,19.04422 1.70800781,20.1134033 C2.23806765,20.6276855 2.85372923,21.0281677 3.5234375,21.303772 L3.5234375,38.84375 C3.5234375,41.6870728 5.8363647,44 8.6796875,44 L29.0787659,44 C28.2358398,43.2054138 27.1867981,42.155365 26.1390991,40.9458619 C26.0279846,40.8179626 25.9188842,40.6900635 25.8114624,40.5625 L8.6796875,40.5625 C7.73202512,40.5625 6.9609375,39.7914124 6.9609375,38.84375 L6.9609375,35.3303833 L22.3088379,35.3303833 C21.7378235,34.1604919 21.328949,33.0137634 21.0818786,31.8928833 L6.9609375,31.8928833 L6.9609375,21.614624 C8.05194087,21.3816529 9.0298157,20.8401794 9.796875,20.0838623 C10.836853,21.1090698 12.2638855,21.7421875 13.8359375,21.7421875 C15.4207458,21.7421875 16.8581848,21.0986634 17.900177,20.059021 C18.9559326,21.0993347 20.4041138,21.7421875 22,21.7421875 C22.3132019,21.7421875 22.6203613,21.7166748 22.9204712,21.6690064 C24.1950989,19.6947937 25.9766236,18.0777588 28.0800781,17.00354 C27.9290162,16.6957093 27.84375,16.349945 27.84375,15.984375 L27.84375,14.1796875 L24.40625,14.1796875 L24.40625,15.8984375 C24.40625,17.2250977 23.3266602,18.3046875 22,18.3046875 C20.6733398,18.3046875 19.59375,17.2250977 19.59375,15.8984375 L19.59375,14.1796875 L16.15625,14.1796875 L16.15625,15.984375 C16.15625,17.2637024 15.1152649,18.3046875 13.8359375,18.3046875 C12.5566101,18.3046875 11.515625,17.2637024 11.515625,15.984375 L11.515625,14.1796875 L8.078125,14.1796875 L8.078125,15.984375 C8.078125,17.2637024 7.03713991,18.3046875 5.7578125,18.3046875 C4.53286745,18.3046875 3.55801394,17.434906 3.44790652,16.2875061 L7.29528809,3.4375 L36.7070618,3.4375 L40.5524292,16.2035827 C40.5228881,16.5157776 40.4309082,16.8101807 40.2895813,17.0743714 C41.3147888,17.6128235 42.2617798,18.2808532 43.1080628,19.0566406 C43.6720276,18.1667175 44,17.1136475 44,15.984375 L44,15.7312623 L39.2616882,0 Z" id="Shape"></path></g></g></g></svg>
                                        <?php }?>
                                    </a>
                                    <h4 class="ml-3 d-inline-block d-md-none"><?php bloginfo('title');?></h4>
                                </div>
                                <div class="col-3 col-md-1">
                                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample03" aria-controls="navbarsExample03" aria-expanded="false" aria-label="Toggle navigation">
                                        <i class="bx bx-menu bx-md"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- <svg width="53px" height="53px" viewBox="0 0 53 53" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <desc>Created with Sketch.</desc> <defs> <path d="M23,46 C10.2975,46 0,35.7025 0,23 C0,10.2975 10.2975,0 23,0 C35.7025,0 46,10.2975 46,23 C46,35.7025 35.7025,46 23,46 Z M23,32 C27.9706,32 32,27.9706 32,23 C32,18.0294 27.9706,14 23,14 C18.0294,14 14,18.0294 14,23 C14,27.9706 18.0294,32 23,32 Z" id="path-1"></path> <linearGradient x1="9.375%" y1="16.5%" x2="9.375%" y2="187.166667%" id="linearGradient-3"> <stop stop-color="#FF48DB" offset="0%"></stop> <stop stop-color="#FFED5D" offset="100%"></stop> </linearGradient> <path d="M23,46 C10.2975,46 0,35.7025 0,23 C0,10.2975 10.2975,0 23,0 C35.7025,0 46,10.2975 46,23 C46,35.7025 35.7025,46 23,46 Z M23,32 C27.9706,32 32,27.9706 32,23 C32,18.0294 27.9706,14 23,14 C18.0294,14 14,18.0294 14,23 C14,27.9706 18.0294,32 23,32 Z" id="path-4"></path> <linearGradient x1="11.0718188%" y1="19.1666667%" x2="9.55333675%" y2="16.2494852%" id="linearGradient-6"> <stop stop-color="#FF48DB" offset="0%"></stop> <stop stop-color="#FFED5D" offset="100%"></stop> </linearGradient> <path d="M6,12 C2.6863,12 0,9.314 0,6 C0,2.686 2.6863,0 6,0 C9.3137,0 12,2.686 12,6 C12,9.314 9.3137,12 6,12 Z" id="path-7"></path> <linearGradient x1="8.5%" y1="15.3333333%" x2="8.5%" y2="186%" id="linearGradient-9"> <stop stop-color="#FFF980" offset="0%"></stop> <stop stop-color="#FFC119" offset="100%"></stop> </linearGradient> <path d="M6,12 C2.6863,12 0,9.314 0,6 C0,2.686 2.6863,0 6,0 C9.3137,0 12,2.686 12,6 C12,9.314 9.3137,12 6,12 Z" id="path-10"></path> <linearGradient x1="8.7346065%" y1="15.4612256%" x2="9.0669775%" y2="16.1976335%" id="linearGradient-12"> <stop stop-color="#FFF980" offset="0%"></stop> <stop stop-color="#FFC119" offset="100%"></stop> </linearGradient> </defs> <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <g id="client's-dashboard" transform="translate(-136.000000, -184.000000)"> <g id="Group-113" transform="translate(143.000000, 191.000000)"> <g id="Group-109"> <mask id="mask-2" fill="white"> <use xlink:href="#path-1"></use> </mask> <g id="Clip-108"></g> <polygon id="Fill-107" fill="url(#linearGradient-3)" mask="url(#mask-2)" points="-143 1009 1457 1009 1457 -191 -143 -191"></polygon> </g> <g id="Group-112"> <mask id="mask-5" fill="white"> <use xlink:href="#path-4"></use> </mask> <g id="Clip-111"></g> <polygon id="Fill-110" fill="url(#linearGradient-6)" mask="url(#mask-5)" points="-143 1009 1457 1009 1457 -191 -143 -191"></polygon> </g> </g> <g id="Group-120" transform="translate(136.000000, 184.000000)"> <g id="Group-116"> <mask id="mask-8" fill="white"> <use xlink:href="#path-7"></use> </mask> <g id="Clip-115"></g> <polygon id="Fill-114" fill="url(#linearGradient-9)" mask="url(#mask-8)" points="-136 1016 1464 1016 1464 -184 -136 -184"></polygon> </g> <g id="Group-119"> <mask id="mask-11" fill="white"> <use xlink:href="#path-10"></use> </mask> <g id="Clip-118"></g> <polygon id="Fill-117" fill="url(#linearGradient-12)" mask="url(#mask-11)" points="-136 1016 1464 1016 1464 -184 -136 -184"></polygon> </g> </g> </g> </g></svg> -->
                        </div>

                        <div class="collapse navbar-collapse" id="navbarsExample03">
                            <ul id="sidebarnav" class="nav mt-md-3 flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="#booking">
                                        <i class='bx bx-calendar-event'></i>
                                        <?php _e('Make a booking','skeda-business');?>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#services">
                                        <i class='bx bx-book-open'></i>
                                        <?php _e('Services','skeda-business');?>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="#contact">
                                        <i class='bx bxs-contact'></i>
                                        <?php _e('Contact','skeda-business');?>
                                    </a>
                                </li>
                                <?php if(is_user_logged_in()){?>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php bloginfo('url');?>/dashboard">
                                        <i class='bx bx-user'></i>
                                        <?php _e('My Dashboard','skeda-business');?>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo wp_logout_url();?>">
                                        <i class='bx bx-log-out'></i>
                                        <?php _e('Log out','skeda-business');?>
                                    </a>
                                </li>
                                <?php } else {?>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo wp_login_url();?>">
                                        <i class='bx bx-user'></i>
                                        <?php _e('Log in','skeda-business');?>
                                    </a>
                                </li>

                                <?php }?>
                            </ul>
                        </div>
                    </div>
                </nav>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10">
                <div class="image-salons my-4 d-flex align-content-center p-5 <?php echo ($cover != '') ? "withimage" : "";?>" style='<?php if($cover != '') { echo "background-image: url($cover)";}?>'>
                    <h1 class="text-white d-none d-md-block"><?php echo skeda_getcompany('skeda_data_name');?></h1>
                </div>
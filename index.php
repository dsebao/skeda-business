<?php

get_header();
if (have_posts()) : while (have_posts()) : the_post();
?>
        <div class="front-main-container">
            <div class="px-md-5 py-md-3">
                <h2><?php _e('Make a Booking','skeda-business');?></h2>

                <?php echo do_shortcode('[ameliabooking]');?>

                <div class="my-md-5">
                    <h2><?php _e('Available Services','skeda-business');?></h2>
                    <div class="row mt-md-3">
                        <?php echo getServices();?>
                    </div>
                </div>

                <div class="my-md-5">
                    <h2 class="mb-md-3"><?php _e('Information','skeda-business');?></h2>
                    <?php echo getLocations();?>
                </div>
            </div>
        </div>
<?php endwhile; endif;?>
<?php get_footer();?>
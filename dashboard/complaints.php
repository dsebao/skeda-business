<?php

checklogin('/wp-admin/');

global $theuser;
$u = getAmeliaUser();

if(current_user_can('administrator')){
    wp_redirect(home_url('/wp-admin/'));
}

/*
Template Name: Complaints
*/

get_header('dash');

if (have_posts()) : while (have_posts()) : the_post();
?>
        <div class="container-gray px-2 px-md-0 py-4 py-md-0">
            <div class="container p-md-5 mb-3">
                

                <div class="message">
                    <?php echo printMessage();?>
                </div>

                <div class="row">
                    <div class="col-md-7">
                        <div class="card mb-5">
                            <div class="card-header">
                                <h3><?php _e('Complains','skeda-business');?></h3>
                            </div>
                            <div class="card-body">
                                <p><?php _e('Please send us a message about your complaint to help you as soon as possible.','skeda-business');?></p>
                                <form action="" method="post">
                                    <div class="form-group">
                                        <label for=""><?php _e('Subject','skeda-business');?></label>
                                        <input type="text" class="form-control" name="subject" required placeholder="<?php _e('Subject','skeda-business');?>">
                                    </div>

                                    <div class="form-group">
                                        <label for="comment"><?php _e('Comment','skeda-business');?></label>
                                        <textarea name="comment" id="comment" rows="5" class="form-control" placeholder="<?php _e('Leave your message','skeda-business');?>" required></textarea>
                                    </div>
                                    <input type="hidden" name="amelia_user_id" value="<?php echo $u['id'];?>"/>
                                    <input type="hidden" name="wp_user_id" value="<?php echo $theuser->ID;?>"/>
                                    <?php wp_nonce_field('security_form', 'security_form_complains');?>
                                    <button type="submit" class="btn btn-secondary rounded-pill"><?php _e('Submit complaint','skeda-business');?></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php endwhile; endif;?>
<?php get_footer();?>
<?php

checklogin('/wp-admin/');

global $theuser;
$u = getAmeliaUser();

if(current_user_can('administrator')){
    wp_redirect(home_url('/wp-admin/'));
}

/*
Template Name: Bills
*/
global $theuser;
get_header('dash');

if (have_posts()) : while (have_posts()) : the_post();
?>
        <div class="container-gray px-2 px-md-0 py-4 py-md-0">
            <div class="container p-md-5 mb-3">
                <div class="row">
                    <div class="col-md-7">
                        <div class="card mb-5">
                            <div class="card-header">
                                <h3><?php _e('Bills','skeda-business');?></h3>
                            </div>
                            <div class="card-body">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php endwhile; endif;?>
<?php get_footer();?>
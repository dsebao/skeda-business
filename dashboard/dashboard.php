<?php

checklogin('/wp-admin/');

global $theuser;

$u = getAmeliaUser();

//Get all bookings
$upcomingbookings = getUpcomingBookings($u['id']);
$previousbookings = getPreviousBookings($u['id']);

$date_format = get_option( 'date_format' );
$time_format = get_option( 'time_format' );

/*
Template Name: Dashboard
*/
global $theuser;
get_header('dash');

if (have_posts()) : while (have_posts()) : the_post();
?>
        <div class="container-gray px-2 px-md-4 py-4 py-md-4">
            <div class="row">
                <div class="col-md-7">
                    <div class="card mb-5">
                        <div class="card-header d-flex">
                            <h3><?php _e('Upcoming Visits','skeda-business');?></h3>
                            <a class='py-2 ml-auto' href='<?php echo get_bloginfo('url');?>/#booking'><?php _e('New booking','skeda-business');?></a>
                        </div>
                        <div class="card-body">
                            <ul class="list-group ameliahelper amelia-list-booking">
                            <?php
                            if(!empty($upcomingbookings)) :
                                foreach ($upcomingbookings as $r => $a) {
                                    $d = get_date_from_gmt($a['bookingStart'], $date_format . " " . $time_format);
                                    echo "<li class='list-group-item'><div class='d-flex align-items-center'><h4 class='mr-auto'>{$a['name']}</h4><span class='text-gray mr-3'>" . __('$','skeda-business') ." {$a['price']}</span><a data-toggle='collapse' href='#show-info-{$a['appointmentId']}' role='button' aria-expanded='false' aria-controls='show-info-{$a['appointmentId']}' class='badge badge-primary badge-pill badge-lg'>".__('Details','skeda-business')."</a></div><span class='text-uppercase small badge mr-2 badge-secondary'>" . __($a['status'],'skeda-business') . "</span> <span class='time'>$d</span>";
                                    echo listBookingCollapse($a);
                                    echo "</li>";
                                }
                            else :
                                echo "<p>" . __('There isn´t upcoming booking.','skeda-business') . "</p>";
                            endif;

                            ?>
                            </ul>
                        </div>
                        <p><a href="<?php bloginfo('url');?>/dashboard/upcoming" class="btn btn-link"><?php _e('See more','skeda-business');?></a></p>
                    </div>

                    <div class="card mb-5">
                        <div class="card-header">
                            <h3><?php _e('Previous visits','skeda-business');?></h3>
                        </div>
                        <div class="card-body">
                            <ul class="list-group ameliahelper amelia-list-booking">
                            <?php
                            if(!empty($previousbookings)) :
                                foreach ($previousbookings as $r => $a) {
                                    $d = get_date_from_gmt($a['bookingStart'], $date_format . " " . $time_format);
                                    $listitem = "<li class='list-group-item'>";
                                    $listitem .= "<div class='d-flex justify-content-between align-items-center'><b>{$a['name']}</b>";
                                    //$listitem .= "<a href='#' data-toggle='modal' data-target='#modal-rate-{$a['id']}' class='btn btn-link btn-link-primary'>Rate the visit</a>";
                                    $listitem .= "<a data-toggle='collapse' href='#show-info-{$a['appointmentId']}' role='button' aria-expanded='false' aria-controls='show-info-{$a['id']}' class='badge badge-primary badge-pill badge-lg'>".__('Details','skeda-business')."</a></div><span class='time'>$d</span>";
                                    $listitem .= listBookingModal($a);
                                    $listitem .= listBookingCollapse($a);
                                    $listitem .= "</li>";
                                    echo $listitem;
                                }
                            else :
                                _e('There isn´t past booking','skeda-business');
                            endif;
                            ?>
                            </ul>
                        </div>
                        <p><a href="<?php bloginfo('url');?>/dashboard/previous/" class="btn btn-link"><?php _e('See more','skeda-business');?></a></p>
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="card mb-5">
                        <div class="card-header color">
                            <h3><?php _e('My profile','skeda-business');?></h3>
                            <p>
                                <?php _e('Username','skeda-business');?>: <?php echo $theuser->user_login;?><br>
                            </p>
                        </div>
                        <div class="card-body">
                            <div class="mt-2 mb-4 text-center">
                                <img src="<?php echo get_avatarimg_url(get_avatar($theuser->ID));?>" width="90" height="90" class="avatar rounded-circle" alt="<?php echo $theuser->display_name;?>">
                            </div>
                            <div class="mb-md-4 mb-3">
                                <p class="line-height-2">
                                    <?php echo $u['firstName'];?> <?php echo $u['lastName'];?><br>
                                    <?php echo $u['email'];?><br>
                                    <?php echo $u['phone'];?> <br>
                                </p>
                            </div>
                            <a href="<?php bloginfo('url');?>/my-account/" class="btn btn-light rounded-pill"><?php _e('Change profile','skeda-business');?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php endwhile; endif;?>
<?php get_footer();?>
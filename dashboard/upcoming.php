<?php

checklogin('/wp-admin/');

global $theuser;

if(current_user_can('administrator')){
    wp_redirect(home_url('/wp-admin/'));
}

$u = getAmeliaUser();

$booking = getUpcomingBookings($u['id']);

$date_format = get_option( 'date_format' );
$time_format = get_option( 'time_format' );

/*
Template Name: Upcoming
*/
global $theuser;
get_header('dash');

if (have_posts()) : while (have_posts()) : the_post();
?>
        <div class="container-gray px-2 px-md-0 py-4 py-md-0">
            <div class="container p-md-5 mb-3">
                <div class="row">
                    <div class="col-md-7">
                        <div class="card mb-5">
                            <div class="card-header d-flex">
                                <h3><?php _e('Upcoming Visits','skeda-business');?></h3>
                                <a class='py-2 ml-auto' href='<?php echo get_bloginfo('url');?>/#booking'><?php _e('New booking','skeda-business');?></a>
                            </div>
                            <div class="card-body">
                                <ul class="list-group ameliahelper amelia-list-booking">
                                <?php
                                if(!empty($booking)) :
                                    foreach ($booking as $r => $a) {
                                        $d = get_date_from_gmt($a['bookingStart'], $date_format . " " . $time_format);
                                        $listitem = "<li class='list-group-item'>";
                                        $listitem .= "<div class='d-flex justify-content-between align-items-center'><b>{$a['name']}</b>";
                                        //$listitem .= "<a href='#' data-toggle='modal' data-target='#modal-rate-{$a['id']}' class='btn btn-link btn-link-primary'>Rate the visit</a>";
                                        $listitem .= "<a data-toggle='collapse' href='#show-info-{$a['appointmentId']}' role='button' aria-expanded='false' aria-controls='show-info-{$a['appointmentId']}' class='badge badge-primary badge-pill badge-lg'>".__('Details','skeda-business')."</a></div><span class='time'>$d</span>";
                                        $listitem .= listBookingModal($a);
                                        $listitem .= listBookingCollapse($a);
                                        $listitem .= "</li>";
                                        echo $listitem;
                                    }

                                else :
                                    _e('There isn´t previous visits','skeda-business');
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php endwhile; endif;?>
<?php get_footer();?>
<?php

/*
Template Name: Home
*/

$a = get_option('skedacompany');

get_header();
if (have_posts()) : while (have_posts()) : the_post();
?>
        <div class="container-gray">
            <div class="px-md-5 py-md-3">
            	<h2 id="booking"><?php _e('Make a Booking','skeda-business');?></h2>
                <?php echo do_shortcode('[ameliabooking]');?>

                <div class="my-md-5">
                	<h2 id="services"><?php _e('Available Services','skeda-business');?></h2>
                	<div class="my-3">
					   <?php echo do_shortcode('[ameliacatalog]');?>
					</div>

				</div>

				<div class="my-md-5">
                    <h2 id="contact" class="mb-md-3"><?php _e('About Us','skeda-business');?></h2>
                    <?php ?>
                    <div class="boxwhite mb-4 ameliahelper ameliahelper_locations">
                        <h2><?php echo $a['skeda_data_name'];?></h2>
                        <p>
                            <?php echo (isset($a['skeda_data_phone']) && $a['skeda_data_phone'] != '') ? "<i class='bx bxs-phone'></i> " . $a['skeda_data_phone'] . "<br>": "";?>
                            <?php echo (isset($a['skeda_data_address']) && $a['skeda_data_address'] != '') ? "<i class='bx bxs-map'></i> " . $a['skeda_data_address'] . "<br>": "";?>
                            <?php echo (isset($a['skeda_data_city']) && $a['skeda_data_city'] != '') ? "<i class='bx bxs-city'></i> " . $a['skeda_data_city'] . "<br>": "";?>
                        </p>
                    </div>
					<h2 id="contact" class="mb-md-3"><?php _e('Locations','skeda-business');?></h2>
					<?php echo getLocations();?>
				</div>
            </div>
        </div>
<?php endwhile; endif;?>
<?php get_footer();?>
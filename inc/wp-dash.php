<?php

/**
 * Always redirect Dashboard page to Amelia Dashboard
 *
 * @return void
 */
function filterUsersDashboard(){
    if(current_user_can('administrator')){
        if ( preg_match('#wp-admin/?(index.php)?$#', $_SERVER['REQUEST_URI']) ) {
            wp_redirect( admin_url('admin.php?page=wpamelia-dashboard') );
        }
    } elseif(current_user_can('wpamelia-customer')){
        wp_redirect(home_url('/dashboard/'));
    } else {
        if ( preg_match('#wp-admin/?(index.php)?$#', $_SERVER['REQUEST_URI']) ) {
            wp_redirect( admin_url('admin.php?page=wpamelia-calendar') );
        }
    }
}
add_action( 'admin_init','filterUsersDashboard');


/**
 * Clean widgets in home dashboard
 *
 * @return void
 */
function disable_default_dashboard_widgets(){
	remove_meta_box('dashboard_right_now', 'dashboard', 'core');
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'core');
	remove_meta_box('dashboard_incoming_links', 'dashboard', 'core');
	remove_meta_box('dashboard_plugins', 'dashboard', 'core');
	remove_meta_box('dashboard_quick_press', 'dashboard', 'core');
	remove_meta_box('dashboard_primary', 'dashboard', 'core');
	remove_meta_box('dashboard_secondary', 'dashboard', 'core');
	remove_meta_box('dashboard_recent_drafts', 'dashboard', 'core');
	//remove_meta_box('dashboard_right_now', 'dashboard', 'core');
}
add_action('admin_menu', 'disable_default_dashboard_widgets');

/**
 * Remove all menus and pages in dash
 *
 * @return void
 */
function remove_menus() {
    if (!is_super_admin()) {
        remove_menu_page( 'upload.php' );
        remove_menu_page( 'themes.php' );
        remove_menu_page( 'edit-comments.php' );
        remove_menu_page( 'edit.php' );
        remove_menu_page( 'tools.php' );
        remove_menu_page( 'edit.php?post_type=page' );
        remove_menu_page( 'index.php' );
        remove_menu_page( 'plugins.php' );
        remove_menu_page( 'options-general.php' );
        remove_menu_page( 'users.php' );
    }
}
add_action( 'admin_menu', 'remove_menus' );

/**
 * Remove the wpadminbar links and bookmarks
 *
 * @param [object] $wp_admin_bar
 * @return void
 */
function remove_from_admin_bar($wp_admin_bar) {
    if ( ! is_super_admin() ) {
        $wp_admin_bar->remove_node('updates');
        $wp_admin_bar->remove_node('comments');
        $wp_admin_bar->remove_node('new-content');
        $wp_admin_bar->remove_node('wp-logo');
        //$wp_admin_bar->remove_node('site-name');
        $wp_admin_bar->remove_node('my-sites');
        $wp_admin_bar->remove_node('edit');
        $wp_admin_bar->remove_node('search');
        //$wp_admin_bar->remove_node('customize');
    }
    $wp_admin_bar->remove_node('wp-logo');
}
add_action('admin_bar_menu', 'remove_from_admin_bar', 999);


/**
 * Create custom pages menus in Dashboard
 *
 * @return void
 */
function register_skeda_custom_menu_page() {
    add_menu_page(__( 'View Site', 'skeda-business' ),
        __('View Site','skeda-business'),
        'amelia_read_calendar',
        get_bloginfo('url'),
        '',
        "dashicons-admin-site-alt3",
        10
    );
    add_menu_page(__( 'Log out', 'skeda-business' ),
        __('Log Out','skeda-business'),
        'amelia_read_calendar',
        wp_logout_url(),
        '',
        "dashicons-redo",
        140
    );
    add_menu_page(__( 'Dashboard', 'skeda-business' ),
        __('Dashboard','wpamelia'),
        'amelia_read_dashboard',
        'admin.php?page=wpamelia-dashboard',
        '',
        "dashicons-home",
        2
    );
    add_menu_page(__( 'Calendar', 'wpamelia' ),
        __('Calendar','wpamelia'),
        'amelia_read_calendar',
        'admin.php?page=wpamelia-calendar#/calendar',
        '',
        "dashicons-calendar",
        3
    );
    add_menu_page(__( 'Appointments', 'wpamelia' ),
        __('Appointments','wpamelia'),
        'amelia_read_appointments',
        'admin.php?page=wpamelia-appointments#/appointments',
        '',
        "dashicons-appointments",
        3
    );
    add_menu_page(__( 'Customers', 'wpamelia' ),
        __('Customers','wpamelia'),
        'amelia_read_customers',
        'admin.php?page=wpamelia-customers#/customers',
        '',
        "dashicons-customers",
        3
    );
    add_menu_page(__( 'Employees', 'wpamelia' ),
        __('Employees','wpamelia'),
        'amelia_read_employees',
        'admin.php?page=wpamelia-employees#/employees',
        '',
        "dashicons-employees",
        3
    );
    add_menu_page(__( 'Services', 'wpamelia' ),
        __('Services','wpamelia'),
        'amelia_read_services',
        'admin.php?page=wpamelia-services#/services',
        '',
        "dashicons-services",
        3
    );
    add_menu_page(__( 'Locations', 'wpamelia' ),
        __('Locations','wpamelia'),
        'amelia_read_locations',
        'admin.php?page=wpamelia-locations#/locations',
        '',
        "dashicons-locations",
        3
    );
    add_menu_page(__( 'Finance', 'wpamelia' ),
        __('Finance','wpamelia'),
        'amelia_read_finance',
        'admin.php?page=wpamelia-finance#/finance',
        '',
        "dashicons-finance",
        5
    );
    add_menu_page(__( 'Settings', 'wpamelia' ),
        __('Settings','wpamelia'),
        'amelia_read_settings',
        'admin.php?page=wpamelia-settings#/settings',
        '',
        "dashicons-settings",
        10
    );
    add_menu_page(__( 'Profile'),
        __('Profile'),
        'read',
        'profile.php',
        '',
        "dashicons-admin-users",
        8
    );
    add_menu_page(__( 'Customize', 'wpamelia' ),
        __('Customize','wpamelia'),
        'amelia_read_customize',
        'admin.php?page=wpamelia-customize#/customize',
        '',
        "dashicons-customize",
        8
    );
}
add_action( 'admin_menu', 'register_skeda_custom_menu_page' );


/**
 * Remove Amelia Page menu
 *
 * @return void
 */
function wpse_136058_remove_menu_pages() {
    remove_menu_page( 'amelia' );
}
add_action( 'admin_init', 'wpse_136058_remove_menu_pages' );


/**
 * Remove submenu pages in options
 *
 * @return void
 */
function skeda_remove_admin_submenus() {
    remove_submenu_page( 'options-general.php', 'wp-offload-ses' );
    remove_submenu_page( 'options-general.php','options-discussion.php');
    remove_submenu_page( 'options-general.php','options-writing.php');
    remove_submenu_page( 'options-general.php','options-reading.php');

    remove_submenu_page( 'options-general.php','options-reading.php');
}
add_action( 'admin_menu', 'skeda_remove_admin_submenus', 999 );


function my_footer_shh() {
    remove_filter( 'update_footer', 'core_update_footer' );
}

add_action( 'admin_menu', 'my_footer_shh' );

/*
Remove Footer
*/
add_filter( 'admin_footer_text', '__return_false' );


/**
 * Create custom meta boxes for business page using CMB2 utility
 *
 * @return void
 */
function skeda_optiondata() {
    $cmb_options = new_cmb2_box( array(
        'id'           => 'skedacompany',
        'title'        => __('Business','skeda-business'),
        'object_types' => array( 'options-page' ),
        'option_key'      => 'skedacompany', // The option key and admin menu page slug.
        'icon_url'        => 'dashicons-networking', // Menu icon. Only applicable if 'parent_slug' is left empty.
        'capability'      => 'wpamelia-manager', // Cap required to view options-page.
        'position'        => 5,
        'message_cb' => 'skeda_cb_afteroptions'
    ));

    $cmb_options->add_field( array(
        'name' => __('Business information','skeda-business'),
        'type' => 'title',
        'id'   => 'title',
    ) );


    $cmb_options->add_field( array(
        'name' => __('Business Name','skeda-business'),
        'type' => 'text',
        'id'   => 'skeda_data_name',
    ) );

    $cmb_options->add_field( array(
        'name' => __('About the Business','skeda-business'),
        'type' => 'text',
        'id'   => 'skeda_data_about',
    ) );

    $cmb_options->add_field( array(
        'name' => __('Address','skeda-business'),
        'type' => 'text',
        'id'   => 'skeda_data_address',
    ) );

    $cmb_options->add_field( array(
        'name' => __('Address','skeda-business'),
        'type' => 'text',
        'id'   => 'skeda_data_address',
    ));

    $cmb_options->add_field( array(
        'name' => __('City','skeda-business'),
        'type' => 'text',
        'id'   => 'skeda_data_city',
    ));

    $cmb_options->add_field( array(
        'name' => __('Phone','skeda-business'),
        'type' => 'text',
        'id'   => 'skeda_data_phone',
    ));

    $cmb_options->add_field( array(
        'name'    => esc_html__( 'Logo', 'skeda-business' ),
        'desc'    => esc_html__( 'Recommended 500x500', 'skeda-business' ),
        'id'      => 'skeda_data_logo',
        'type'    => 'file',
    ));
    $cmb_options->add_field( array(
        'name'    => esc_html__( 'Cover image', 'skeda-business' ),
        'id'      => 'skeda_data_cover',
        'type'    => 'file',
    ));

    $cmb_options->add_field( array(
        'name' => __('Currency','skeda-business'),
        'type' => 'select',
        'id'   => 'skeda_data_currency',
        'options' => array(
            'USD' => esc_html__( 'US Dollar', 'skeda-business' ),
            'RUB'   => esc_html__( 'Russian Ruble', 'skeda-business' ),
            'EUR'     => esc_html__( 'Euro', 'skeda-business' ),
        ),
    ));

    $cmb_options->add_field( array(
        'name' => __('Site language','skeda-business'),
        'type' => 'select',
        'id'   => 'skeda_data_lang',
        'options' => array(
            '' => esc_html__( 'English', 'skeda-business' ),
            'ru_RU'   => esc_html__( 'Russian', 'skeda-business' ),
        ),
    ));

    $cmb_options->add_field( array(
        'name' => __('Timezone','skeda-business'),
        'id'   => 'skeda_data_time',
        'type' => 'select_timezone'
    ));

    $cmb_options->add_field( array(
        'name' => __('Date format','skeda-business'),
        'id'   => 'skeda_data_date',
        'type' => 'select',
        'options' => array(
            'F j, Y' => date_i18n('F j, Y'),
            'Y-m-d' =>  date_i18n('Y-m-d'),
            'd-m-Y' =>  date_i18n('d-m-Y'),
            'd/m/Y' =>  date_i18n('d/m/Y'),
            'm/d/Y' =>  date_i18n('m/d/Y'),
        ),
    ));

    $cmb_options->add_field( array(
        'name' => __('Time format','skeda-business'),
        'id'   => 'skeda_data_hour',
        'type' => 'select',
        'options' => array(
            'g:i A' =>  date_i18n('g:i A'),
            'H:i' =>  date_i18n('H:i'),
        ),
    ));
}
add_action( 'cmb2_admin_init', 'skeda_optiondata' );

//after save the state of the options run a callback to save some info to the amelia table settings
function skeda_cb_afteroptions( $cmb, $args ) {
    if ( ! empty( $args['should_notify'] ) ) {
        if ( $args['is_updated'] ) {

            //Get amelia settings data
            $amelia = json_decode(get_option('amelia_settings'),true);

            $amelia['company']['name'] = skeda_getcompany('skeda_data_name');
            $amelia['company']['pictureFullPath'] = skeda_getcompany('skeda_data_logo');
            $amelia['company']['address'] = skeda_getcompany('skeda_data_address');
            $amelia['company']['phone'] = skeda_getcompany('skeda_data_phone');
            $amelia['payments']['currency'] = skeda_getcompany('skeda_data_currency');

            update_option('amelia_settings',json_encode($amelia));

            update_option('blogname',skeda_getcompany('skeda_data_name'));
            update_option('blogdescription',skeda_getcompany('skeda_data_about'));

            $args['message'] = sprintf( esc_html__( '%s &mdash; Updated!', 'cmb2' ), $cmb->prop( 'title' ) );

            

            if(get_option('WPLANG') !== skeda_getcompany('skeda_data_lang')){
                update_option('WPLANG',skeda_getcompany('skeda_data_lang'));
                wp_redirect(admin_url() . "admin.php?page=skedacompany");
            }

            update_option('date_format',skeda_getcompany('skeda_data_date'));
            update_option('time_format',skeda_getcompany('skeda_data_hour'));

            $time = skeda_getcompany('skeda_data_time');

            if(strstr($time,'/') != false){
                update_option('timezone_string',skeda_getcompany('skeda_data_time'));
                update_option('gmt_offset','');
            } else{
                $t = str_replace('UTC','',$time);
                $a = str_replace('+','',$t);
                update_option('timezone_string','');
                update_option('gmt_offset',$time);
            }

        }

        add_settings_error( $args['setting'], $args['code'], $args['message'], $args['type'] );
    }
}


function skeda_getcompany( $key = '', $default = false ) {
    if ( function_exists( 'cmb2_get_option' ) ) {
        // Use cmb2_get_option as it passes through some key filters.
        return cmb2_get_option( 'skedacompany', $key, $default );
    }
    // Fallback to get_option if CMB2 is not loaded yet.
    $opts = get_option( 'skedacompany', $default );
    $val = $default;
    if ( 'all' == $key ) {
        $val = $opts;
    } elseif ( is_array( $opts ) && array_key_exists( $key, $opts ) && false !== $opts[ $key ] ) {
        $val = $opts[ $key ];
    }
    return $val;
}

/**
 * Manage User Admin Display Table Columns
 *
 * @param Array $columns
 *      [cb]        => <input type="checkbox" />
 *      [username]  => Username
 *      [name]      => Name
 *      [email]     => Email
 *      [role]      => Role
 *      [posts]     => Posts
 *
 * @return Array $columns
 */
function wpseq_270133_users( $columns ) {

    unset( $columns['role'] );
    unset( $columns['posts'] );

    return $columns;
}
add_filter( 'manage_users_columns', 'wpseq_270133_users' );

remove_filter('authenticate', 'wp_authenticate_username_password');

add_filter('gettext', function($text){
    if(in_array($GLOBALS['pagenow'], array('wp-login.php'))){
        if('Username or Email Address' == $text){
            return __('Email','skeda-business');
        }
    }
    return $text;
}, 20);


<?php

/**
 * Create working buttons for email notifications 
 *
 * @param [string] $l The href link
 * @param [string] $t The text of the button
 * @return void
 */
function htmlButton($l,$t){
    $button = '<div><!--[if mso]><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="'.$l.'" style="height:40px;v-text-anchor:middle;width:200px;" arcsize="30%" stroke="f" fillcolor="#21A466"><w:anchorlock/><center><![endif]--><a href="'.$l.'" style="background-color:#21A466;border-radius:40px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:40px;text-align:center;text-decoration:none;width:200px;-webkit-text-size-adjust:none;">'.$t.'</a><!--[if mso]></center></v:roundrect><![endif]--></div>';
    return $button;
}

/**
 * Remove capabilities from editors.
 *
 * @return void
 *
 */
function wpcodex_set_capabilities() {
    $editor = get_role( 'administrator' );

    $caps = array(
        'switch_themes',
    );

    foreach ( $caps as $cap ) {
        $editor->remove_cap( $cap );
    }
}
add_action( 'init', 'wpcodex_set_capabilities' );

/**
 * Change the admin title page
 *
 * @param [string] $admin_title
 * @param [string] $title
 * @return void
 */
function my_admin_title($admin_title, $title){
    return get_bloginfo('name').' &bull; '.$title;
}
add_filter('admin_title', 'my_admin_title', 10, 2);


/**
 * Define default settings in amelia plugin
 *
 * This function will replace in every site some variable data related to the Russian skeda
 * 
 * @return void
 */
function ameliaSettingsDefault(){

    //Get the user data
    global $theuser;
    $databusiness = get_user_meta($theuser->ID,'business_data');

    //get all amelia settings
    $a = get_option('skedacompany');

    //If not defined skeda data name save all previous data in skeda company option row
    if(!isset($a['skeda_data_name'])){
        update_option('skedacompany',array(
            'skeda_data_name' => $databusiness[0]['business'],
            'skeda_data_phone' => $databusiness[0]['phone'],
            'skeda_data_time' => 'UTC+3',
            'skeda_data_currency' => 'RUB',
            'skeda_data_hour' => 'H:i',
            'skeda_data_lang' => 'ru_RU',
            'skeda_data_date' => 'd-m-Y'
            )
        );

        update_option('timezone_string', 'Europe/Moscow');
        update_option('blogdescription', '');
    }

    //Decode all amelia setting to json
    $amelia = json_decode(get_option('amelia_settings'),true);
    //Gmaps Api key

    //defined google Maps Api Key in every multisite skeda
    $amelia['general']['gMapApiKey'] = 'AIzaSyD8hCGWh1NE6rMbNOGEZNeXzKwj6hSi9Qg';
    
    //Define time slot length 5 minutes
    $amelia['general']['timeSlotLength'] = 900;

    //Check if amelia settings exist
    if(isset($amelia['company']['name'])){

        if(isset($amelia['company']['name']) && $amelia['company']['name'] == '')
            $amelia['company']['name'] = $databusiness[0]['business'];

        if(isset($amelia['notifications']['senderName']) && $amelia['notifications']['senderName'] == '')
            $amelia['notifications']['senderName'] = 'Skēda | ' . $theuser->display_name;

        if(isset($amelia['notifications']['senderEmail']) && $amelia['notifications']['senderEmail'] == '')
            $amelia['notifications']['senderEmail'] = 'noreply@skeda.ru';

        //Notifications settings
        $amelia['notifications']['mailService'] = 'wp_mail';
        $amelia['notifications']['smtpHost'] = 'email-smtp.us-west-2.amazonaws.com';
        $amelia['notifications']['smtpPort'] = '587';
        $amelia['notifications']['smtpSecure'] = 'tls';
        $amelia['notifications']['smtpUsername'] = 'AKIAZMMMJ3UJ5XS7EXFW';
        $amelia['notifications']['smtpPassword'] = 'BITXo+pT0DQE0dDDPeqg/1oDQZH8I+DivLEMSph1lwfQ';

        $amelia['roles']['allowConfigureSchedule'] = true;
        $amelia['roles']['allowConfigureDaysOff'] = true;
        $amelia['roles']['allowConfigureSpecialDays'] = true;
        $amelia['roles']['allowWriteAppointments'] = true;
        $amelia['roles']['automaticallyCreateCustomer'] = true;
        $amelia['roles']['allowCustomerReschedule'] = true;
        $amelia['roles']['allowWriteEvents'] = true;
        $amelia['roles']['inspectCustomerInfo'] = true;
        $amelia['labels']['enabled'] = false;
        $amelia['payments']['currency'] = 'RUB';
        $amelia['payments']['symbol'] = '₽';

        $a = update_option('amelia_settings',json_encode($amelia,true));
    }
}
add_action( 'admin_init', 'ameliaSettingsDefault',1);


/**
 * Detect if exist attached files to the request
 *
 * @param [string] $name The input file name
 * @return void
 */
function any_uploaded($name){
  	if(is_array($_FILES[$name]['error'])){
    	foreach ($_FILES[$name]['error'] as $ferror) {
      		return ($ferror != UPLOAD_ERR_NO_FILE) ? true : false;
    	}
  	} else {
    	return ($_FILES[$name]['error'] != UPLOAD_ERR_NO_FILE) ? true : false;
  	}
}

function get_avatarimg_url($get_avatar){
    preg_match('#src=["|\'](.+)["|\']#Uuis', $get_avatar, $matches);
    return $matches[1];
}

function custom_avatars($avatar, $id_or_email, $size){
  $image_url = get_user_meta($id_or_email, '_avatar', true);
  if($image_url !== '' && is_int($id_or_email))
    $return = '<img src="'.$image_url.'" class="avatar-img" width="'.$size.'" height="'.$size.'"/>';
  elseif($avatar)
    $return = $avatar;
  return $return;
}
add_filter('get_avatar', 'custom_avatars', 10, 5);


add_filter('get_avatar_url', 'set_https', 10, 3);
function set_https($url, $id_or_email, $args){
    return set_url_scheme( $url, 'https' );;
}

/**
 * $_GET helper
 *
 * @param [string] $g Variable in url
 * @return string Empty string if is not set
 */
function getparams($g){
    if(isset($_GET[$g])){
        return $_GET[$g];
    } else {
        return '';
    }
}


/**
 * Check if user is logged
 *
 * @param string $where optional redirect link
 * @return void
 */
function checklogin($where = ''){
	if (!is_user_logged_in()) {
  		wp_redirect(home_url() . $where);
	} else {
        if(current_user_can('administrator')){
            wp_redirect(home_url('/wp-admin/'));
        }
    }
}


//Redirect Customer after login and block him/her to the dashboard
function login_redirect_based_on_roles($user_login, $user) {
    if( in_array( 'wpamelia-customer',$user->roles ) ){
        exit( wp_redirect(home_url('/dashboard/')));
    }
}
add_action( 'wp_login', 'login_redirect_based_on_roles', 10, 2);


function rearrange_files($arr) {
    foreach($arr as $key => $all) {
        foreach($all as $i => $val) {
            $new_array[$i][$key] = $val;
        }
    }
    return $new_array;
}

function printMessage(){
    if(isset($_GET['action']) && $_GET['action'] != ''){
        switch ($_GET['action']) {
        case 'update-profile':
            echo "<div class='alert alert-success'>".__('Profile updated','skeda-business')."</div>";
        break;
        case 'send-complains':
            echo "<div class='alert alert-success'>".__('Complaint successfully sended to the manager.','skeda-business')."</div>";
        break;
        case 'error-complains':
            echo "<div class='alert alert-warning'>".__('An error ocurred, please try again.','skeda-business')."</div>";
        default:
        break;
    }
  }
}

/**
 * Custom error page
 *
 * @param [string] $message Message
 * @param string $title
 * @param array $args
 * @return void
 */
function themed_wp_die_handler( $message, $title = '', $args = array() ) {
    $defaults = array( 'response' => 500 );
    $r = wp_parse_args($args, $defaults);
    if ( function_exists( 'is_wp_error' ) && is_wp_error( $message ) ) {
        $errors = $message->get_error_messages();
        switch ( count( $errors ) ) {
            case 0 :
                $message = '';
                break;
            case 1 :
                $message = $errors[0];
                break;
            default :
                $message = "<ul>\n\t\t<li>" . join( "</li>\n\t\t<li>", $errors ) . "</li>\n\t</ul>";
                break;
        }
    } else {
        $message = $message;
    }
    require_once get_stylesheet_directory() . '/wp-die.php';
    die();
}
add_filter( 'wp_die_handler', function( $handler ) { return 'themed_wp_die_handler';}, 10);


function loadStyleUser(){
    $options = getAmeliaColors();
    echo '
    <style>
        .mainsite-background{
            background-color: '.$options['primaryColor'].' !important;
        }
        .mainsite-text{
            color: '.$options['textColor'].' !important;
        }
        .mainsite-textonbg{
            color: '.$options['textColorOnBackground'].' !important;
        }
        .mainsite-grad{
            background: '.$options['primaryColor'].' !important;
            background: linear-gradient(to right, '.$options['primaryGradient1'].' 0%, '.$options['primaryGradient2'].' 100%);
        }
    </style>';
}

add_action('wp_head', 'loadStyleUser');

function listBookingModal($a){
    $star = '';
    $html = '<div class="modal fade" id="modal-rate-'.$a['appointmentId'].'" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title">'.__('Rate this visit','skeda-business').'</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            	<span aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16.192 6.344L11.949 10.586 7.707 6.344 6.293 7.758 10.535 12 6.293 16.242 7.707 17.656 11.949 13.414 16.192 17.656 17.606 16.242 13.364 12 17.606 7.758z"/></svg></span>
            </button>
          </div>
          <div class="modal-body">
          </div>
        </div>
      </div>
    </div>';
	return $html;
}

function listBookingCollapse($a){
    $u = getProvider($a['providerId']);
    $html = '<div class="collapse show-info-collapse" id="show-info-'.$a['appointmentId'].'"><div class="bg-graycolor rounded p-4 mt-4">';

    $html .= '<b>' . __('Service description','skeda-business') . "</b><br><p>" . $a['description'] . "</p>";
    $html .= '<b>' . __('Employee','skeda-business') . "</b><br>" . $u['firstName'] . " " .$u['lastName'] . "</p>";

    $html .= '</div></div>';
    return $html;
}

/*
    Remove wordpress in mail subject
*/
function res_fromname($email){
    $wpfrom = "Skēda | " . get_option('blogname');
    return $wpfrom;
}
add_filter('wp_mail_from_name', 'res_fromname');

function use_noresponder(){
  return 'noreply@skeda.ru';
}
add_filter( 'wp_mail_from', 'use_noresponder' );

?>
<?php

add_filter("retrieve_password_message", "skeda_custom_password_reset", 99, 4);

function skeda_custom_password_reset($message, $key, $user_login, $user_data )    {
	$btn = htmlButton(network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_login), 'login'),__('Reset password','skeda'));
	$message = "<p>".__('Someone has requested a password reset for the following account:','skeda-business') . "</p><b>" . sprintf(__('%s'), $user_data->user_email) . "</b><p>" . __('If this was a mistake, just ignore this email and nothing will happen. To reset your password, visit the following address:','skeda-business') . "</p>" .  '' . $btn. "\r\n" . "<p>" . __('If you have any further issues, please email us to info@skeda.ru','skeda-business') . "</p>";
	return $message;
}


add_filter( 'wp_new_user_notification_email', 'custom_wp_new_user_notification_email', 10, 3 );

function custom_wp_new_user_notification_email( $wp_new_user_notification_email, $user, $blogname ) {
    $wp_new_user_notification_email['headers'] = array('Content-Type: text/html; charset=UTF-8');

    $key = get_password_reset_key( $user );
    $message = "<h2>" . sprintf(__('Welcome to','skeda-business')) . " " .$blogname . "</h2>";
    $message .= __('To set your password, visit the following address:','skeda-business') . "<br><br>";
    $message .= htmlButton(network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user->user_login), 'login'),__('Create password','skeda-business')) . "<br>";
    $message .= __('After this you can log to our web.','skeda-business');
    $wp_new_user_notification_email['message'] = $message;

    return $wp_new_user_notification_email;
}

/*
    Function to facility emails notifications in WP
*/

add_filter( 'wp_mail', 'my_wp_mail_filter' );
function my_wp_mail_filter( $args ) {
    $thebody = $GLOBALS['emailtemplate'];

    $tpl = str_replace('{{content}}', "\n". $args['message'] . "\n", $thebody);
    $tpl = str_replace("\n", '<br>', $tpl);

    //$message = strip_tags( $message );
    // Decode body
    $message = wp_specialchars_decode( $tpl, ENT_QUOTES );

    $new_wp_mail = array(
        'to'          => $args['to'],
        'subject'     => $args['subject'],
        'message'     => $message,
        'headers'     => $args['headers'],
        'attachments' => $args['attachments'],
    );

    return $new_wp_mail;
}


add_filter( 'wp_mail_content_type','mycustom_set_content_type' );
function mycustom_set_content_type() {
        return "text/html";
}

function sendNotification($subject,$content,$email){

    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
    $headers .= 'From: '.$GLOBALS['SITENAME'].' <noreply@'.$GLOBALS['DOMAIN'].'>' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

    $sent = wp_mail($email, $subject, $content, $headers);
    if($sent){
        return $sent;
    } else {
        return false;
    }
}

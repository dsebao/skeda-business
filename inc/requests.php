<?php


//SignIn
if (isset($_POST['security_form_profile']) && wp_verify_nonce($_POST['security_form_profile'], 'security_form' )){
    $errores = array();

    global $theuser;
    $element = array();

    (isset($_POST['firstname']) && $_POST['firstname'] != "") ? $element['firstName'] =  esc_attr($_POST['firstname']) : $errores[] = __('Your first name can´t be empty','skeda-business');
    (isset($_POST['lastname']) && $_POST['lastname'] != "") ? $element['lastName'] =  esc_attr($_POST['lastname']) : $errores[] = __('Your last name can´t be empty','skeda-business');

    if(isset($_POST['email']) && $_POST['email'] != "" && is_email($_POST['email'])){
        if(email_exists($_POST['email']) && $_POST['email'] != $theuser->user_email)
            $element['email'] = esc_attr($_POST['email']);
    } else {
        $errores[] = __('Your email isn´t valid','skeda-business');
    }


    (isset($_POST['phone']) && $_POST['phone'] != "") ? $element['phone'] =  $_POST['phone'] : $errores[] = __('Your phone can´t be empty','skeda-business');

    if(isset($_POST['pass']) && $_POST['pass'] != ""){
        wp_update_user( array( 'ID' => $theuser->ID, 'user_pass' => esc_attr( $_POST['pass'])));
    }

    if(empty($errores)){
        $dataupdate = array(
            'ID' => $theuser->ID,
            'display_name' => $element['firstName'] . ' ' . $element['lastName'],
            'user_nicename' => $element['firstName'] . ' ' . $element['lastName'],
        );
        if(isset($element['email'])){
            $dataupdate['user_email'] = $element['email'];
        }

        $idu = wp_update_user($dataupdate);

        if($_FILES['avatar-file'] != ''){
            require_once ABSPATH.'wp-admin/includes/file.php';
            $status = wp_handle_upload($_FILES['avatar-file'], array('test_form' => false));
            if(empty($status['error'])){
                $img = wp_get_image_editor($status['file']);
                if(!is_wp_error($img)){
                    $resize = $img->resize( 120, 120,true);
                    if(isset($exif) && isset($exif['Orientation']) == 8)
                        $saved = $img->rotate(90);
                    if(isset($exif) && isset($exif['Orientation']) == 6)
                        $saved = $img->rotate(-90);
                    $saved = $img->save();
                    $uploads = wp_upload_dir();
                    $resized_url = $uploads['url'].'/'.basename($saved['file']);
                    update_user_meta($theuser->ID, '_avatar',$resized_url);
                    updateAmeliaUser($theuser->ID,array('pictureThumbPath' => $resized_url));
                }
            } else{
                $errores[] = __('Error uploading your image','skeda-business');
            }
        }

        $id = updateAmeliaUser($theuser->ID,$element);

        if($idu != 0){
            wp_redirect(home_url('dashboard/my-account?action=update-profile'));
        } else {
            wp_redirect(home_url('dashboard/my-account?action=error-profile'));
        }
    } else {
        $error_message = '';
        foreach ($errores as $v){
            $error_message .= "<span>".$v."</span>";
        }
        $message = "<div class='alert alert-warning'>$error_message</div>";
    }
}

if (isset($_POST['security_form_complains']) && wp_verify_nonce($_POST['security_form_complains'], 'security_form' )){
    $errores = array();

    (isset($_POST['subject']) && $_POST['subject'] != "") ? $subject =  esc_attr($_POST['subject']) : $errores[] = __('Your subject can´t be empty','skeda-business');
    (isset($_POST['comment']) && $_POST['comment'] != "") ? $comment =  esc_attr($_POST['comment']) : $errores[] = __('Your comment can´t be empty','skeda-business');

    if(isset($_POST['wp_user_id']) && intval($_POST['wp_user_id']) && empty($errores)){
        $u = getAmeliaUser(intval($_POST['wp_user_id']));

        $content = "<h2>" . __( 'New complaint from a customer', 'skeda-business' ). "</h2><p>" . __('User', 'skeda-business' ) . ": ".$u['firstName']. " " . $u['lastName'] . "<br>".__('Email').": ".$u['email']."<br>".__('Subject','skeda-business').": " . $subject . "<br>".__('Comment','skeda-business').": ".$comment."<br><br></p><br>";
        $a = sendNotification(__('New complaint from a customer','skeda-business') ." ".$u['email'],$content,get_option('admin_email'));

        if($a){
            wp_redirect(home_url('dashboard/complaints?action=send-complains'));
        } else {
            wp_redirect(home_url('dashboard/complaints?action=error-complains'));
        }
    } else {
        $error_message = '';
        foreach ($errores as $v) {
            $error_message .= "<span>".$v."</span>";
        }
        $message = "<div class='alert alert-warning'>$error_message</div>";
    }

}



?>
<?php

/**
 * Detect in front end if amelia plugin is activated detecting if one shortcode is generated, this is the most economic mode to do it
 *
 * @return void Error if no amelia booking plugin is set up, force to login to the dashboard
 */
function detectAmeliaTables(){
	if (!shortcode_exists('ameliabooking') && !is_admin() && $GLOBALS['pagenow'] != 'wp-login.php' && $GLOBALS['pagenow'] != 'wp-activate.php'){
		$message = __('Please login to the dashboard to initialize your site','skeda-business');
		$btn = "<a style='margin:20px 0' href='".get_admin_url()."'>".__('Login','skeda-business')."</a>";
		wp_die($message . $btn);
	}
}
add_action('init','detectAmeliaTables');


/**
 * Replace notifications strings hardcoded to the amelia database (table _amelia_notifications )
 *
 * @return void
 */
function replaceTranslatedNotifications(){
	if(get_option('notifications_translated') != 'yes'){

		$notif = array(
			array('customer_appointment_approved','Вы записаны в %company_name%','<strong>%customer_full_name%,</strong><br><br>Вы записаны в <strong>%company_name%</strong><br><br>Детали записи:<br><br><strong>%appointment_date_time%<br><br>%service_name%<br><br>%employee_full_name%<br><br>%appointment_price%<br><br>%location_address%</strong><br><br>Спасибо за Ваш выбор! Чтобы иметь удобный доступ к информации о Ваших визитах, используйте личный кабинет.<br>Если у Вас его ещё нет, <strong>мы отправили Вам ссылку для его создания в другом письме!</strong><br><br>%company_name%'),
			array('customer_appointment_pending','Запись ожидает подтверждения салона %company_name%','<strong>%customer_full_name%,</strong><br><br>Ваша запись в <strong>%company_name%</strong> ожидает подтверждения салона.<br><br>Детали записи: <br><br><strong>%appointment_date_time%<br><br>%service_name%<br><br>%employee_full_name%<br><br>%appointment_price%<br><br>%location_address%</strong><br><br>Спасибо за Ваш выбор!Чтобы иметь удобный доступ к информации о Ваших визитах, используйте личный кабинет.<br>Если у Вас его ещё нет, <strong>мы отправили Вам ссылку для его создания в другом письме!</strong><br><br>%company_name%'),
			array('customer_appointment_rejected','Запись в %company_name% отменена','<strong>%customer_full_name%,</strong><br><br>Ваша запись в <strong>%company_name%</strong> отменена салоном.<br><br>Для уточнения информации свяжитесь с салоном: <strong>%company_phone%</strong>.<br><br>%company_name%'),
			array('customer_appointment_canceled','Запись в %company_name% отменена','<strong>%customer_full_name%,</strong><br><br>Ваша запись в <strong>%company_name%</strong> отменена.<br><br>%company_name%'),
			array('customer_appointment_rescheduled','Запись в %company_name% перенесена','<strong>%customer_full_name%,</strong><br><br>Ваша запись в <strong>%company_name%</strong> перенесена. Обновленная информация о записи:<br><br><strong>%appointment_date_time%<br><br>%service_name%<br><br>%employee_full_name%<br><br>%appointment_price%<br><br>%location_address%<br><br></strong>Пожалуйста, если Ваши планы поменяются, свяжитесь с нами:<strong>%company_phone%</strong>.<br><br>%company_name%'),
			array('customer_appointment_next_day_reminder','Напоминание о записи в %company_name%','<strong>%customer_full_name%,</strong><br><br>Напоминаем, что Вы записаны в <strong>%company_name%</strong>:<br><br><strong>%appointment_date_time%<br><br>%service_name%<br><br>%employee_full_name%<br><br>%appointment_price%<br><br>%location_address%<br><br></strong>Пожалуйста, если Ваши планы поменяются, свяжитесь с нами: <strong>%company_phone%</strong>.<br><br>%company_name%'),
			array('customer_appointment_follow_up','Благодарим Вас за визит!','<strong>%customer_full_name%!</strong><br><br>Ещё раз благодарим Вас за выбор нашего салона %company_name%. Мы надеемся, что Вы довольны оказанными услугами. Если у Вас остались пожелания, пожалуйста, отправьте их из своего личного кабинета. <br><br>Ждём Вас снова!<br>%company_name%'),
			array('customer_birthday_greeting','Поздравляем с Днём рождения!','<strong>%customer_full_name%!</strong><br><br>Поздравляем Вас с Днём рождения и желаем бесконечной гармонии и красоты в Вашей жизни!<br><br>Всего самого наилучшего,<br>%company_name%'),
			array('provider_appointment_approved','У Вас новая запись','Здравствуйте, <strong>%employee_full_name%</strong>,<br><br>У Вас есть новая запись:<br><br><strong>%customer_full_name%</strong><br><br><strong>%service_name%</strong><br><br><strong>%location_name%<br><br><strong></strong>%appointment_date_time%</strong><br><br>Запись добавлена в ваш календарь.<br><br>Спасибо,<br><strong>%company_name%</strong>'),
			array('provider_appointment_pending','Новая запись. Ожидает подтверждения салона','Здравствуйте, <strong>%employee_full_name%</strong>,<br><br>У Вас есть новая запись, ожидающая подтверждения салона:<br><br><strong>%customer_full_name%</strong><br><br> <strong>%service_name%</strong><br><br><strong>%location_name%<br><br><strong></strong>%appointment_date_time%</strong><br><br>Спасибо,<br><strong>%company_name%</strong>'),
			array('provider_appointment_rejected','Запись отменена','Здравствуйте, <strong>%employee_full_name%</strong>.<br><br>Запись к Вам была отменена:<br><br><strong>%customer_full_name%</strong><br><br><strong>%service_name%</strong><br><br><strong>%location_name%<br><br><strong></strong>%appointment_date_time%</strong><br><br>Спасибо,<br><strong>%company_name%</strong>'),
			array('provider_appointment_canceled','Запись отменена','Здравствуйте, <strong>%employee_full_name%</strong>,<br><br>Запись к Вам была отменена:<br><br><strong>%customer_full_name%</strong><br><br><strong>%service_name%</strong><br><br><strong>%location_name%<br><br><strong></strong>%appointment_date_time%</strong><br><br>Спасибо,<br><strong>%company_name%</strong>'),
			array('provider_appointment_rescheduled','Запись перенесена','Здравствуйте, <strong>%employee_full_name%</strong>,<br><br> Запись клиента <strong>%customer_full_name%</strong> к Вам была перенесена.<br><br>Новое время записи:<br><br> <strong>%service_name%</strong><br><br><strong>%location_name%<br><br><strong></strong>%appointment_date_time%</strong><br><br>Спасибо,<br><strong>%company_name%</strong>'),
			array('provider_appointment_next_day_reminder','Напоминание о записи в %company_name%','Здравствуйте, <strong>%employee_full_name%</strong>,<br><br>Напоминаем о предстоящей записи:<br><br><strong>%customer_full_name%</strong><br><br><strong>%service_name%</strong><br><br><strong>%location_name%<br><br><strong></strong>%appointment_date_time%</strong><br><br><strong>%company_name%</strong>'),
		);

		global $wpdb;
		foreach($notif as $not){
			$query = $wpdb->update($wpdb->base_prefix.$GLOBALS['IDSITE']."_amelia_notifications",array('subject' => $not[1],'content' => $not[2]),array('name' => $not[0]));
		}

		update_option('notifications_translated','yes');
	}
}

add_action('admin_init', 'replaceTranslatedNotifications');



/**
 * Get all business available services
 *
 * @param string $class Bootstrap column class
 * @return string Html
 */
function getServices($class = 'col-md-4'){
    // get his posts 'ASC'
    global $wpdb;
    $idblog = $GLOBALS['IDSITE'];
    $query = $wpdb->prepare("SELECT * FROM {$wpdb->base_prefix}%d_amelia_services",$idblog);
    $results = $wpdb->get_results($query,'ARRAY_A');
    $html = '';
	if(!empty($results) && is_array($results)){
		foreach ($results as $a) {
			$html .= "<div class='$class'><div class='ameliahelper ameliahp_services'>{$a['name']}</div></div>";
		}
	}
	return $html;
}

/**
 * Get all business available locations
 *
 * @param string $class Bootstrap column class
 * @return string Html
 */
function getLocations($class = 'col-md-12'){
    // get his posts 'ASC'
    global $wpdb;
    $idblog = $GLOBALS['IDSITE'];
    $query = $wpdb->prepare("SELECT * FROM {$wpdb->base_prefix}%d_amelia_locations WHERE status=%s",$idblog,'visible');
    $results = $wpdb->get_results($query,'ARRAY_A');
    $html = '';
	if(!empty($results) && is_array($results)){
		foreach ($results as $a) {
			$html .= '<div class="boxwhite mb-md-4 mb-2 ameliahelper ameliahelper_locations"><div class="row"><div class="col-md-6">';
			$html .= '<h2>'.$a['name'].'</h2>';
			$html .= '<i class="bx bxs-map"></i> '.$a['address'].'<br>';
			$html .= '<i class="bx bxs-phone"></i> '.$a['phone'].'<br>';
			$html .= '<p>'.$a['description'].'<p>';
			$html .= '</div><div class="col-md-6">';
			$html .= '<img src="'.$a['pictureFullPath'].'" class="w-100 rounded">';
			$html .= '</div></div></div>';
		}
	}
	return $html;
}

/**
 * Get Amelia User data by ID
 *
 * @param int $id Wordpress Id of the user (not the amelia user ID)
 * @return string return html
 */
function getAmeliaUser($id = ''){
	if($id == ''){
		global $theuser;
		$id = $theuser->ID;
	}

    // get his posts 'ASC'
    global $wpdb;
    $idblog = $GLOBALS['IDSITE'];
    $query = $wpdb->prepare("SELECT * FROM {$wpdb->base_prefix}%d_amelia_users WHERE externalId = %d",$idblog,$id);
    $results = $wpdb->get_results($query,'ARRAY_A');
	$html = array();

	if(!empty($results) && is_array($results)){
		foreach ($results as $a) {
			$html[] = $a;
		}
	} else {
		$html[0] = __('User not found','skeda-business');
	}
	return $html[0];
}

/**
 * Get User Provider
 *
 * @param int $id Id of the Wp User
 * @return string User data
 */
function getProvider($id = ''){
    // get his posts 'ASC'
    global $wpdb;
    $idblog = $GLOBALS['IDSITE'];
    $query = $wpdb->prepare("SELECT * FROM {$wpdb->base_prefix}%d_amelia_users WHERE id = %d AND type = %s",$idblog,$id,'provider');
    $results = $wpdb->get_results($query,'ARRAY_A');
    $html = array();
	if(!empty($results) && is_array($results)){
		foreach ($results as $a) {
			$html[] = $a;
		}
	} else {
		$html[0] = __('User not found','skeda-business');
	}
	return $html[0];
}


/**
*
* Update Amelia Users
*
* @id
* @param array The user id
* @return
*
*/
function updateAmeliaUser($id,$data){
    global $wpdb;
    $idblog = $GLOBALS['IDSITE'];

    //The element that will be updated
    $query = $wpdb->update(
		$wpdb->base_prefix . $idblog . '_amelia_users',
		$data,
		array( 'externalId' => $id )
	);
	return $query;
}

/**
 * Get Users bookings
 *
 * @param [type] $user User id
 * @param string $number Number of bookings (not implemented in the sql)
 * @return array Booking Data
 */
function getBookings($user,$number = '5'){
	global $wpdb;
    $idblog = $GLOBALS['IDSITE'];
    $query = $wpdb->prepare("SELECT * FROM {$wpdb->base_prefix}%d_amelia_customer_bookings AS cb INNER JOIN {$wpdb->base_prefix}%d_amelia_appointments As ap INNER JOIN {$wpdb->base_prefix}%d_amelia_services As aserv ON cb.appointmentId = ap.id AND aserv.id = ap.serviceId WHERE customerId = %d",$idblog,$idblog,$idblog,$user);
    $results = $wpdb->get_results($query,'ARRAY_A');
	return $results;
}

/**
 * Get Upcoming bookings
 *
 * @param int $user User ID WP
 * @param string $number Number of bookings (not implemented in the sql)
 * @return array Booking data
 */
function getUpcomingBookings($user,$number = '5'){
	global $wpdb;
    $idblog = $GLOBALS['IDSITE'];
    $query = $wpdb->prepare("SELECT *, ap.status FROM {$wpdb->base_prefix}%d_amelia_customer_bookings AS cb INNER JOIN {$wpdb->base_prefix}%d_amelia_appointments As ap INNER JOIN {$wpdb->base_prefix}%d_amelia_services As aserv ON cb.appointmentId = ap.id AND aserv.id = ap.serviceId WHERE customerId = %d AND ap.bookingStart >= NOW()",$idblog,$idblog,$idblog,$user);
    $results = $wpdb->get_results($query,'ARRAY_A');
	return $results;
}

/**
 * Get Previous Booking
 *
 * @param int $user User ID WP
 * @param string $number Number of bookings (not implemented in the sql)
 * @return array Booking data
 */
function getPreviousBookings($user,$number = '5'){
	global $wpdb;
    $idblog = $GLOBALS['IDSITE'];
    $query = $wpdb->prepare("SELECT * FROM {$wpdb->base_prefix}%d_amelia_customer_bookings AS cb INNER JOIN {$wpdb->base_prefix}%d_amelia_appointments As ap INNER JOIN {$wpdb->base_prefix}%d_amelia_services As aserv ON cb.appointmentId = ap.id AND aserv.id = ap.serviceId WHERE customerId = %d AND ap.bookingStart <= NOW()",$idblog,$idblog,$idblog,$user);
    $results = $wpdb->get_results($query,'ARRAY_A');
	return $results;
}

/**
 * Get Amelia Business Data
 *
 * @return array
 */
function getAmeliaBusinessData(){
    $amelia = json_decode(get_option('amelia_settings'),true);
    return $amelia;
}

/**
 * Get Amelia Colors Settings from the dashbaord
 *
 * @return void
 */
function getAmeliaColors(){
    $a = get_option('amelia_settings');
    $b = json_decode($a,true);
    return $b['customization'];
}







<?php

include_once(get_stylesheet_directory() . '/inc/core.php');

/*  Get the bootstrap! */
if ( file_exists(  __DIR__ . '/inc/cmb2/init.php' ) ) {
	require_once  __DIR__ . '/inc/cmb2/init.php';
}


include_once(get_stylesheet_directory() . '/inc/app.php');
include_once(get_stylesheet_directory() . '/inc/amelia-helper.php');

include_once(get_stylesheet_directory() . '/inc/wp-dash.php');
include_once(get_stylesheet_directory() . '/inc/extras.php');
include_once(get_stylesheet_directory() . '/inc/mailnotifications.php');

include_once(get_stylesheet_directory() . '/inc/requests.php');
include_once(get_stylesheet_directory() . '/inc/ajax.php');

?>